function install_flintrock_wrapper {
    local LOCAL_BIN="${HOME}/.local/bin"

    if [ ! -e "${LOCAL_BIN}" ]; then
        mkdir -p "${LOCAL_BIN}"
    fi

    if [ -z ${path[(r)${LOCAL_BIN}]:+x} ]; then
        export PATH=${PATH}:${LOCAL_BIN}
    fi

    local PROJECT="${0:A:h}"
    (
        cd "${PROJECT}"

        if ! pipenv --venv >&/dev/null; then
            pipenv install
        fi
    )

    local COMMAND="${LOCAL_BIN}/flintrock"

    if [ ! -e "${COMMAND}" ]; then
        ln -s "${PROJECT}/bin/runner" "${COMMAND}"
    fi
}

if ! which flintrock >/dev/null 2>&1; then
    install_flintrock_wrapper
fi

unfunction install_flintrock_wrapper

