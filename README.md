Flintrock Pipenv Wrapper
========================

This is a simple demonstration of using a pipenv and a script to run a command within an environment.
This allows you to use the [flintrock command line interface](https://github.com/nchammas/flintrock) without installing it globally.
This uses pipenv to install it into a virtual environment and uses a shim to load that environment when invoking `flintrock`.

Antigen
-------

You can install this with [antigen](https://github.com/zsh-users/antigen):

```bash
antigen bundle https://gitlab.com/matthewfranglen/flintrock-pipenv-wrapper.git
```

This makes the `flintrock` command available.

Synopsis
--------

This assumes that `~/.local/bin` is in your `$PATH` _after_ the pipenv entries.

```bash
➜ git clone git@gitlab.com:matthewfranglen/flintrock-pipenv-wrapper.git
➜ cd flintrock-pipenv-wrapper
➜ pipenv install
➜ ln -s "${PWD}/bin/runner" ~/.local/bin/flintrock
➜ flintrock
Usage: flintrock [OPTIONS] COMMAND [ARGS]...

  Flintrock

  A command-line tool for launching Apache Spark clusters.

Options:
  --config TEXT         Path to a Flintrock configuration file.
  --provider [ec2]
  --version             Show the version and exit.
  --debug / --no-debug  Show debug information.
  --help                Show this message and exit.

Commands:
  add-slaves     Add slaves to an existing cluster.
  configure      Configure Flintrock's defaults.
  copy-file      Copy a local file up to a cluster.
  describe       Describe an existing cluster.
  destroy        Destroy a cluster.
  launch         Launch a new cluster.
  login          Login to the master of an existing cluster.
  remove-slaves  Remove slaves from an existing cluster.
  run-command    Run a shell command on a cluster.
  start          Start an existing, stopped cluster.
  stop           Stop an existing, running cluster.
```
